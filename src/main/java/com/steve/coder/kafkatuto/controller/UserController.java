package com.steve.coder.kafkatuto.controller;


import com.steve.coder.kafkatuto.entity.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("kafka")
public class UserController {

    //@Autowired
    //private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplateForString;

    @Autowired
    private KafkaTemplate<String, Mail> kafkaTemplate;
    private static final String TOPIC = "Kafka_Example";
    private static final String TOPIC2 = "Kafka_Example_json";

    @GetMapping("/publish/{message}")
    public String post (@PathVariable("message") final String message){

        kafkaTemplateForString.send(TOPIC, message);
        return "Published successfully!";
    }

    @GetMapping("/publisher/{subject}")
    public String sendMail (@PathVariable("subject") final String subject){
        kafkaTemplate.send(TOPIC2, new Mail(subject, "Mail", new Date()));
      //  kafkaTemplate.send(TOPIC, message);
        return "mail Published successfully!";
    }
}
