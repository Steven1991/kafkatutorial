package com.steve.coder.kafkatuto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaTutoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaTutoApplication.class, args);
	}

}
