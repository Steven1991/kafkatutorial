package com.steve.coder.kafkatuto.entity;


import java.util.Date;

public class Mail {

    private String subject;
    private  String object;
    private Date date;

    public Mail() {
    }

    public Mail(String subject, String object, Date date){
        this.subject = subject;
        this.object = object;
        this.date = new Date();
    }

    public Mail(String subject, String object){
        this.subject = subject;
        this.object = object;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "subject='" + subject + '\'' +
                ", object='" + object + '\'' +
                          '}';
    }
}
