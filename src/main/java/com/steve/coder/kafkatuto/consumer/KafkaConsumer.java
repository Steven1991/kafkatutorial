package com.steve.coder.kafkatuto.consumer;

import com.steve.coder.kafkatuto.entity.Mail;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @KafkaListener(topics ="Kafka_Example", groupId =  "group_id")
    public void consumer (String message) {
        System.out.println("Consume message: " +message);
    }

    @KafkaListener(topics ="Kafka_Example_json", groupId =  "group_json", containerFactory = "mailKafkaListenerFactory")
    public void consumeJson (Mail mail) {
        System.out.println("Consumed JSON message: " +mail);
    }


}
